require 'bourbon'
require 'kath-style/version'

gem_path = File.expand_path(File.join(File.dirname(__FILE__), ".."))
stylesheets_path = File.join(gem_path, 'stylesheets')

::SassC.load_paths << stylesheets_path
