# Version is a number. If a version contains alphas, it will be created as a prerelease version
# Date is in the form of YYYY-MM-DD
module KathStyle
  VERSION = "0.0.0-dev"
  DATE = "2017-01-18"
end
